<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Rank Airlines</title>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js" type="text/javascript"></script>
		<script src="http://cdn.jquerytools.org/1.2.5/all/jquery.tools.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?= base_url();?>javascript/bsn.AutoSuggest_2.1.3_comp.js" charset="utf-8">
		</script>
		<script type="text/javascript" src="<?= base_url();?>javascript/jquery.rater.packed.js" charset="utf-8">
		</script>
		<script type="text/javascript" src="<?= base_url();?>javascript/jquery.input_hint.js" charset="utf-8">
		</script>
		<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js" charset="utf-8"></script>
		<script type="text/javascript" src="<?= base_url();?>javascript/jquery.pagination.js" charset="utf - 8">
		</script>
		<script type="text/javascript" src="<?= base_url();?>javascript/jquery.timeago.js" charset="utf - 8">
		</script>
		<!-- Dependency for classes -->
		<script type="text/javascript" src="<?= base_url();?>javascript/DUI.js" charset="utf - 8">
		</script>
		<script type="text/javascript" src="<?= base_url();?>javascript/module/ranking.js" charset="utf - 8">
		</script>
		<script type="text/javascript" src="<?= base_url();?>javascript/module/main.js" charset="utf - 8">
		</script>
		<script type="text/javascript" src="<?= base_url();?>javascript/jquery.reveal.js" charset="utf-8">
		</script>
		<link href='http://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?= base_url();?>css/autosuggest_inquisitor.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="<?= base_url();?>css/rater.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="<?= base_url();?>css/main.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="<?= base_url();?>css/pagination.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="<?= base_url();?>css/bubbles.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="<?= base_url();?>css/reveal.css" type="text/css" media="screen" charset="utf-8" />
		<script id="comment_item" type="text/html">
			<p class="triangle-right">${comment}</p><p class="sub_author">${fullname}  ${date} , gave ${ranking}/5</p>
		</script>
		<style>
			h1 { font-family: 'Cabin', arial, serif; }
			.tooltip {
			display:none;
			background:transparent url(<?= base_url();?>img/tooltip/black_arrow.png);
			font-size:12px;
			height:70px;
			width:160px;
			padding:25px;
			color:#fff;
			}

			/* the overlayed element */
			

			#list3 {
			font: normal 1.1em Arial, Helvetica, sans-serif;
			color: #666;
			}
			#list3 p {
			font: normal .7em Arial, Helvetica, sans-serif;
			color: #000;
			border-left: solid 1px #999;
			margin: 0;
			padding: 0 0 1em 1em;
			}
		</style>
		<script type="text/javascript">

//add curvy corners
var settings = {
tl: { radius: 10 },
tr: { radius: 10 },
bl: { radius: 10 },
br: { radius: 10 },
antiAlias: true,
autoPad: true
}

$(document).ready (function() {

$("#addCommentsBtn").attr("disabled", true);
$("#airlineTxt").tooltip({

// tweak the position
offset: [10, 2],

// use the "slide" effect
effect: 'slide'

// add dynamic plugin with optional configuration for bottom edge
}).dynamic({ bottom: { direction: 'down', bounce: true } });

var options = {
script: function (input) { return "<?= base_url();?>ranking/search_airline/"+input; },
varname:"input",
json:true,
shownoresults:true,
noresults: "Airline not found",
maxresults:6,
meth:"GET",
callback: function (obj) {

$("#aid").val(obj.id);

var stars = loadStars(obj.id);
loadComments(obj.id);

//enable comment button
$("#addCommentsBtn").attr("disabled", false);

}
}; // end of Options setup

var as_json = new bsn.AutoSuggest('airlineTxt', options);

$('input[hint]').inputHint();

}); //ON DOM ready

function loadStars(airid){
var stars = 0;
stars =  onSelectGetStars("<?= base_url();?>ranking/getstars/",airid, function(data){

//do the following once Onselect appears
$('.star-rating').remove();
$('#starRanking').rater('<?= base_url();?>ranking/postRanking/'+airid, {style: 'basic', curvalue:data});
$("#message").text("");

$("#commentBox").val("");

}); //stars value preload
return stars;
}//end loadStars

function loadComments(airid){
//Load comments
var total = 10;
onLoadCommentsTotal('<?= base_url();?>ranking/listCommentsCount/', airid,function(data){
total = data;
if (total > 0){

$("#comment_counter").text(total+ " comments");
$("#Pagination").show();
$("#commentsHeader").text('User comments');
$('#commentsResult').show();

$("#Pagination").pagination(total, {
num_edge_entries: 2,
num_display_entries: 5,
callback: pageselectCallback,
items_per_page:5
});
}else{
$("#comment_counter").empty();
$("#commentsHeader").text("No comments , add your 2 cents");
$('#commentsResult').hide();
$("#Pagination").hide();
}
});
} //end loadComments

function addComment(){

onClickPostComment('<?= base_url();?>ranking/postComment',function(code,data){
//callback

if (code == 'OK'){
//message posted
$("#addCommentsBtn").val("Comments Posted, thanks.");

//REload the comments
var airlineid = $("#aid").val();
console.log("reload comments after post "+airlineid);
loadComments(airlineid);

$("#airlineTxt").attr('hint','Search another...');

}
});
return false;
}

function pageselectCallback(page_index, jq){
// Get number of elements per pagionation page from form
var items_per_page = 5;
//var totallen = 100;
//var max_elem = Math.min((page_index+1) * items_per_page, totallen);

onLoadComments('<?= base_url();?>
	ranking / listComments / ', $("#aid").val(),page_index*items_per_page,items_per_page,function(json){
	$("#commentsResult").empty();
	//compile template
	$( "#comment_item" ).template("item_tmpl");
	$.each(json, function(i, item) {

		var data = {
			comment : item.comment,
			date : jQuery.timeago(item.dateposted),
			ranking : item.ranking,
			fullname : item.fullname
		};

		//not the fastest
		//$("#com_tmpl").tmpl(data).appendTo("#commentsResult");
		$.tmpl("item_tmpl",data).appendTo("#commentsResult");

	});
	});

	//pass page_index*items_per_page as pgn
	//max_elem first 5 , then 10, 15...
	//pass post up as num_per_page

	/*IN
	[{"id":"6","airlineid":"1","userid":"0","ranking":"1","comment":"","userIP":"207.112.100.152","dateposted":"2009-02-01 03:31:52"},{"id":"10","airlineid":"1","userid":"0","ranking":"3","comment":"testing it again","userIP":"209.183.6.169","dateposted":"2009-02-11 01:10:34"}]
	*/

	// Prevent click eventpropagation
	return false;
	}

	//-->
		</script>
	</head>
	<body >
		<div id="contents" >
			<h1>Your opinion counts, give Airline your star or not...</h1>
			<div id="inputArea">
				<div class="asholder">
					<input hint="Start typing (e.g. Air Canada)" title="Start Typing an Airline"  type="text" id="airlineTxt" value="" class="idle"/>
				</div>
			</div>
			<div id="message"></div>
			<br style="clear:both;" />
			<h3 id="commentsHeader"></h3>
			<br style="clear:both;" />
			<div id="Pagination" class="pagination"></div>
			<br style="clear:both;" />
			<h3 class="commentheading" id="comment_counter"></h3>
			<div id="commentsResult"></div>
			<br style="clear:both;" />
			<div id="starRanking"></div>
			<div id="rating">
				<textarea  hint="Type your comments"  id="commentBox"></textarea>
				<input type="hidden" value="" id="aid"/>
				<input  type="submit" id="addCommentsBtn" value="Post Comment" onclick="addComment();" class="idle"/>
			</div>
			<p>
				<a href="#" data-reveal-id="myModal">Web API (v1)</a>
			</p>
			<div id="myModal" class="reveal-modal">
     <h1>Rank Airline API (V1 )</h1>
    	<p>Default response format is JSON.
						<br/>
						If no data is found, HTTP header 404 will be returned.
						<br/>
					</p>
					<ol id="list3">
						<li>
							<p>
								Airlines: List all airlines (JSON - default ,XML) - <i>http://www.rankairline.com/api/v1/airlines</i>
							</p>
						</li>
						<li>
							<p>
								Airlines: Retrieve specific airline by ID (JSON,XML) - <i>http://rankairline.com/api/v1/airline/id/{ID}/format/json</i>
								<br/>
								(Example: http://rankairline.com/api/v1/airline/id/3/format/json)
							</p>
						</li>
						<li>
							<p>
								Comments: Retrieve latest (by date) comments (JSON,XML) - <i>http://rankairline.com/api/v1/comments_latest</i>
							</p>
						</li>
						<li>
							<p>
								Comments: Retrieve comments (by airline ID parameter) (JSON,XML)
								<br/>
								- <i>http://rankairline.com/api/v1/comments_airline/id/{airline id}/format/json</i>
							</p>
						</li>
						<li>
							<p>
								Comments: Search comments (by term) using a search algorithm (JSON, XML)
								<br/>
								- <i>http://rankairline.com/api/v1/comments_search/term/{text to search by}</i>
								<br/>
								(Example: http://rankairline.com/api/v1/comments_search/term/london/format/xml )
							</p>
						</li>
					</ol>
     <a class="close-reveal-modal">&#215;</a>
</div>
			
		</div>
		<!-- contents -->
	</body>
</html>
