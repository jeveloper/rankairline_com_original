<?php


   class Ranking extends CI_Controller{
       
       
       
   function __construct()
       {
            parent::__construct();
            // Your own constructor code
       }
    
  
  function index(){
    $this->load->view('rank');   
  }  
   
   
    
    
    function spit(){
    
        //http://rankairline.com/api/v1/airline/id/3/format/json
        
        $this->load->library('rest', array('server' => 'http://www.rankairline.com/api/v1/'));
    
    /*
    * parameters
    * 'http_user' => '',  
    'http_pass' => '',  
    'http_auth' => '' // or 'digest'  or basic
    */  

      $id = 3;
  
      $airline = $this->rest->get('airline', array('id' => $id), 'application/json');  
        //var_dump($airline);
        
      echo "SHORT NAME" .$airline[0]->shortname;  
      
      //echo "SHORT NAME 1" .$airline[1]->shortname;  
    }
    
            //Anonymous posting
   function postRanking(){
        
      //check this ranking was already posted
       //if so , do nothing
       // 
       
       //get IP address
        $userip = $this->input->ip_address();
        $airlineId = $this->uri->segment(3); 
        $ranking = $this->input->post('rating');
        $valid = false;
        
        $userid = 0; //later on a registered user 
         
         
          
        if (is_numeric($ranking)){
            if ($ranking >= 0 && $ranking <= 5){
                $valid = true;               
            }
        }
        
        if (is_numeric($airlineId) && $valid){
       
           
           if ($userip != '0.0.0.0'){
                
               $query = $this->db->query("select id from airfeedback where userIP = '$userip' and airlineid = '$airlineId'");
                      
                if ($query->num_rows() == 0){
                    //add ranking
                     
                     
                    $sql = "INSERT INTO airfeedback (airlineid,userid,ranking,comment,userIP)
                    VALUES ($airlineId, $userid,$ranking,'','$userip')";
                     
                    $this->db->query($sql);

                    //echo $this->retrieveStarsTotal($airlineId)." ratings.";
                      
                }
           
           }
     } 
   }
   
   
   function postComment(){
      //comment
      //aid
      
      //get IP address
        $userip = $this->input->ip_address();
        $userid = 0;
        $airlineId = $this->input->post('aid'); 
        $comment = $this->input->post('comment'); 
        
        $defStar = 2;
        
      //if exists, update the existing record
      //find by IP address if not a registered user
      $query = $this->db->query("select id from airfeedback where userIP = '$userip' and airlineid = '$airlineId'"); 
      
       if ($query->num_rows() > 0){  
           $sql = "UPDATE airfeedback SET comment = '$comment' where userIP = '$userip' and airlineid = '$airlineId'";                                                         
           $this->db->query($sql);
          
             
       }else{
            $sql = "INSERT INTO airfeedback (airlineid,userid,ranking,comment,userIP)
                    VALUES ($airlineId, $userid,$defStar,'$comment','$userip')";
                     
                    $this->db->query($sql);
       }
         //echo "OK\n".$this->lang->line("added");  
        echo "OK\n"."Posted"; 
        
   }
   
   function listComments(){
   //listcomments/page/2
   //json
     $airline = $this->uri->segment(3);
     $pagenum = $this->input->post('pgn'); 
     $total = $this->input->post('up');
      //LIMIT 5,10 shows 5-15
      
     if (is_numeric($pagenum) && is_numeric($total)){
            
         if (is_numeric($airline)){
         $query = $this->db->query("select air.*, DATE_FORMAT(air.dateposted, '%e %b, %Y') as dposted , s.fullname from airfeedback air LEFT OUTER JOIN user s  ON air.userid = s.id where airlineid = '$airline' and comment <> '' ORDER BY dateposted DESC LIMIT $pagenum,$total");            
                echo json_encode($query->result_array());  
         }
     }
     
   }
   //print total comments for airlne
   function listCommentsCount(){
        $airline = $this->uri->segment(3);  
        if (is_numeric($airline)){
                $query = $this->db->query("select count(*) as total from airfeedback where airlineid = '$airline' and comment <> ''  ");            
                echo $query->row()->total; 
         }else{
            echo 0;
         } 
   }
   

   
   function recentComments(){
   
       //last 5 postings
     $airline = $this->uri->segment(3); 
       
     if (is_numeric($airline)){
            $query = $this->db->query("select * from airfeedback where  dateposted >= ADDDATE(curdate(), -5) and comment <> '' ");            
            echo json_encode($query->result_array());  
     }
     
   }
   
   function recentRankings(){
   //json
    
    $airline = $this->uri->segment(3);
     
     if (is_numeric($airline)){
            $query = $this->db->query("select * from airfeedback where airlineid = '$airline'");            
            echo json_encode($query->result_array());  
     }
     
   }
   
   
   
   function getStars(){
       $airline = $this->uri->segment(3);
        $stars = 0;
       if (is_numeric($airline)){
            //get average from postings             
             $query = $this->db->query("select round(avg(ranking)) as stars from airfeedback where airlineid = '$airline'");
             $row = $query->row();
             $stars = $row->stars;
             if (!is_numeric($stars))
                $stars = 0;
                
             
       }
       
       echo $stars;
   }
   
   //show how many ratings were there
   function getStarsTotal(){
       $airline = $this->uri->segment(3);
       
         if (is_numeric($airline)){
             
             //get average from postings             
             $query = $this->db->query("select count(*) as total from airfeedback where airlineid = '$airline'");
             
             echo $query->row()->total;          
       }
        
   }
   
    /*
 Searches by short name and long name  return json
 
 @param segment(3) as text
 @returns array of searched results
 
 */
        function search_airline(){
          
             $input = $this->uri->segment(3);
              $this->load->model('Airlinemodel','airline');
              
              
                $aResults = array(); 
               foreach ($this->airline->search($input) as $row)
               {
                    $aResults[] = array( "id"=>$row->id ,"value"=>$row->shortname, "info"=>$row->longname );  
               
               }
              
               echo json_encode(array("results"=>$aResults));           
                   
          }
          
          
          
          
          
            function iscache(){
              
              //i had to change controller to extend from CI_controller and constructor is generic
              //models the same
                
                $data = "test";
            $this->cache->write($data, 'some');

            $t = $this->cache->get('some');
            echo APPPATH.'cache/';
              echo $t;
              
            
              
            }
            
            function testme(){
            $this->load->driver('cache');
            $this->cache->file->save('foo', 'bar', 10);
            }
            
            function testget(){
            $this->load->driver('cache');      
            $foo = $this->cache->file->get('foo');
            echo $foo;
            
            }
            
            
       
}
?>