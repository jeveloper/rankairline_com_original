<?php
  require(APPPATH.'/libraries/REST_Controller.php');  
  class V1 extends REST_Controller
{
    //http://rankairline.com/api/v1/airline/id/3
    function airline_get()
    {
        if(!$this->get('id'))
        {
            $this->response(NULL, 400);
        }

         $this->load->model('Airlinemodel','airline');
        $airline = $this->airline->findById($this->get('id'));
        
       
   /*
   
   buttons
   
   http://www.smashingmagazine.com/2009/12/02/pushing-your-buttons-with-practical-css3/
 
http://www.zurb.com/playground/super-awesome-buttons
 
http://www.zurb.com/playground/radioactive-buttons


   
   
   GET BRIEFED
   
   http://www.accidentalhacker.com/sticky-notes-with-css3/
   
   
   
   
   http://ajaxian.com/archives/jquery-data-binding-templates-and-mobile-john-resig-at-fowa
   
   
    http://www.learningjquery.com/2010/06/autocomplete-migration-guide
    http://jqueryui.com/demos/autocomplete/#custom-data
    
    nice styles:
    http://www.smashingapps.com/2009/06/05/21-stylish-cssjquery-solutions-to-beautify-your-web-designs.html
    
    http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
    
    http://ajaxian.com/archives/jquery-data-binding-templates-and-mobile-john-resig-at-fowa
    
    
         elastic
         
         http://www.unwrongest.com/projects/elastic/#demo
         
         
         
         bottom use jquery tools scrollable with little signs of each airlines thats recently been ranked
         
   */ 
         

        if($airline)
        {
            $this->response($airline, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(NULL, 404);
        }
    }

    /*
    function user_post()
    {
        $result = $this->user_model->update( $this->post('id'), array(
            'name' => $this->post('name'),
            'email' => $this->post('email')
        ));

        if($result === FALSE)
        {
            $this->response(array('status' => 'failed'));
        }

        else
        {
            $this->response(array('status' => 'success'));
        }

    }
      */
     //http://rankairline.com/api/v1/airlines/format/json 
    function airlines_get()
    {
        

         
        $this->load->model('Airlinemodel','airline');
        $airlines = $this->airline->findAll();
        if($airlines)
        {
            $this->response($airlines, 200);
        }

        else
        {
            $this->response(NULL, 404);
        }
    }
    
    
    //inherit from a class for comments
    function comments_latest_get(){
     $result = null;
                                                                                                                      
     $pagenum = 0; 
    $total = 10;
      //LIMIT 5,10 shows 5-15
      
         $query = $this->db->query("select air.*, DATE_FORMAT(air.dateposted, '%e %b, %Y') as dposted , s.fullname from airfeedback air LEFT OUTER JOIN user s  ON air.userid = s.id    ORDER BY dateposted DESC LIMIT $pagenum,$total");            
         if ($query->num_rows() > 0){   
           
         $result = $query->result_array();
         } 
         
         
     
        if($result)
        {
            $this->response($result, 200);
        }
        else
        {
            
            $this->response(NULL, 404);
        }
    
    }
    function comments_airline_get(){
       
       
       if(!$this->get('id'))
        {
            $this->response(NULL, 400);
        }
      
       $airline = $this->get('id');
      
             $result = array();
            
         if (is_numeric($airline)){
         $query = $this->db->query("select air.*, DATE_FORMAT(air.dateposted, '%e %b, %Y') as dposted , s.fullname from airfeedback air LEFT OUTER JOIN user s  ON air.userid = s.id where airlineid = '$airline' and comment <> '' ORDER BY dateposted DESC ");            
         $result = $query->result_array();  
         }
     
          if($result)
        {
            $this->response($result, 200);
        }

        else
        {
            $this->response(NULL, 404);
        }
     
    
    }
    function comments_search_get(){
        if(!$this->get('term'))
        {
            $this->response(NULL, 400);
        }
      
       $term = $this->get('term');
      
         $result = null;
                                                                                                                      
         
         $query = $this->db->query("select air.*, DATE_FORMAT(air.dateposted, '%e %b, %Y') as dposted , s.fullname from airfeedback air LEFT OUTER JOIN user s  ON air.userid = s.id   where match (comment) against ('+$term' in boolean mode) ORDER BY dateposted DESC ");            
         if ($query->num_rows() > 0){   
           
         $result = $query->result_array();
         } 
         
         
     
        if($result)
        {
            $this->response($result, 200);
        }
        else
        {
            
            $this->response(NULL, 404);
        }
     
    
    }
    
    //inherit to read from twitter
      
    
    
}

/*
*   function user_get()  
    {            $data = array('returned: '. $this->get('id'));  
        // respond with information about a user  
    }  
  
    function user_put()  
    {  
     $data = array('returned: '. $this->put('id'));  
        // create a new user and respond with a status/errors  
    }  
  
    function user_post()  
    {  
     $data = array('returned: '. $this->post('id'));  
        // update an existing user and respond with a status/errors  
    }  
  
    function user_delete()  
    {  
     $data = array('returned: '. $this->delete('id'));  
        // delete a user and respond with a status/errors  
    }   
* 
*/
?>
