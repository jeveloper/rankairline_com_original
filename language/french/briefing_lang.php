<?php
  $lang['error_email_missing'] = "You must submit an email address";
  $lang['add_briefing_head'] = "Add your own briefing";
  $lang['add_briefing_btn'] = "Create a briefing";
  $lang['created_briefing'] = "A briefing was created successfully.";
  $lang['incomplete_fields'] = "Please fill out the fields.";
  $lang['briefing_removed'] = "A briefing was removed.";
  $lang['briefing_updated'] = "A briefing was updated.";
  $lang['briefing_na'] = "A briefing is not available.";
  
   $lang['manage_briefings_head'] = "Manage Briefings";
   
   
   
   // FORM labels
   
   
   
   $lang['lbl_title'] = "Title (visible to others)";
   $lang['lbl_alias'] = "Alias (for yourself)";
   $lang['lbl_point'] = "Point text";
   $lang['lbl_privacy'] = "Privacy";
   
   $lang['lbl_privacy_1'] = "Private";
   $lang['lbl_privacy_2'] = "Public";
   $lang['lbl_privacy_3'] = "Group";
   
   $lang['lbl_preview'] = "Preview Window";
   
   $lang['lbl_filter_showall'] = "Show All";
   $lang['lbl_filter_latest'] = "Latest";
   
   $lang['lbl_switch'] = "Switch Styles";    
           
?>
