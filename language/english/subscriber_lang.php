<?php
$lang['add_btn'] = "Add Subscriber/Friend.";
$lang['added'] = "Added Subscriber.";
$lang['deleted'] = "Deleted Subscriber.";
$lang['updated'] = "Updated Subscriber.";
$lang['no_records'] = "There are no friends in your list.";
$lang['page_title'] = "Subscriber List";
?>
