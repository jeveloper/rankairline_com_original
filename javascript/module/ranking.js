
  //Defining a ranking  
 var Ranking = DUI.Class.create({
 
                id : null,
                stars: 0,
                comment: '',
                airlineid: 0,
                
                init: function(){
                   //instantiates automatically
                },
                
                init : function(stars,comment,airlineid)
                {
                //constructor
                    this.setStars(stars);
                    this.setComment(comment);
                    this.setAirlineid(airlineid);
                },

 
                setId : function(id)
                {
                    this.id = id;
                },
 
                getId : function()
                {
                    return this.id;
                },
                
                setStars: function(stars){
                    this.stars = stars;
                },
                getStars: function(){
                    return this.stars;
                },
                setComment: function(comment){
                    this.comment = comment;
                },
                getComment: function(){
                    return this.comment;
                },
                setAirlineid: function(id){
                    this.airlineid = id;
                },
                getAirlineid: function(){
                    return this.airlineid;
                },
                
                PostRank: function(fullUrl){
                    //url to a script
                    $.post(fullUrl, { stars: this.getStars(), aid: this.getAirlineid() },
                          function(data){
                                  
                          });
                },
               
                
                PostComment: function(fullUrl,callback){
                   //airline id
                    //comments
                    //console.log('postcomment comment:'+this.getComment() + ' airlineid:'+this.getAirlineid());
                    
                    //url to a script
                    $.post(fullUrl, { comment: this.getComment(), aid: this.getAirlineid() },
                          function(data){
                             var response = data.split("\n");
                                    if ( response.length == 2 )
                                    {
                                        callback(response[0],response[1]);                                        
                                        
                                    }
                          });
                },
               
                getComments: function(url,page,total,callback){
                //pagenum
                    $.post(url+this.getAirlineid(),{ pgn: page, up: total },
                        function(data){
                          callback(data);
                        },"json");

                },
                 getCommentsTotal: function(serviceurl, callback){
                    
                       
                       $.get(serviceurl+this.getAirlineid(), function(data){
                           callback(data); 
                       });
                 },
                
                
                getAverageStars: function(serviceurl, callback){
                   
                       
                       $.get(serviceurl+this.getAirlineid(), function(data){
                           callback(data); 
                       });
                 }
                
                
            });
            
               
/* USAGE
              var me = new Ranking();
 
            me.setComment('Quentin Zervaas');

*/       

