<?php
  class Usermodel extends CI_Model {

    var $id = 0;     
      
    var $email   = '';
    var $password = "";
    
    var $firstname = '';
    var $lastname    = '';

    
    
 function __construct()
       {
            parent::__construct();
            // Your own constructor code
       }

    function fieldsAll(){
        return 'id, email, firstname, lastname';
    }
    
    
    // returns inserted id
    function insert_user($email,$password,$firstname, $lastname)
    {
        $this->email = $email;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->password = $password;
           
        $this->db->insert('user', $this);
        
        return $this->db->insert_id();
    }

    function delete_user($id){
          $this->db->delete("user",$id);
    }
    
    function update_user($id,$email,$password, $firstname, $lastname)
    {
       $this->email = $email;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->password = $password;

        $this->db->update('user', $this, array('id', $id));
    }
    
    
     function isEmailUsed($email){
           $this->db->select("id")->from("user")->where(array('email'=> trim($email)));
           if ($this->db->get()->num_rows() > 0)
                return true;
            else
                return false;  
     }
    
    function getUser($email,$password){
        
        
        $this->db->select($this->fieldsAll())->from('user')->where(array('email'=> $email,'password' =>$password) );
        return $this->db->get()->result();     
    }
    
    function findByID($id){    
        
        $this->db->select($this->fieldsAll())->from('user')->where('id', $id);
        return $this->db->get()->result(); 
    
    }

}
?>
