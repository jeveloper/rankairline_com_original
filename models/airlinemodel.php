<?php
  class Airlinemodel extends CI_Model{
  
      var $id = 0;
      var $shortname = '';
      var $longname = '';
      var $countrycode = '';
      
      
     function __construct()
       {
            parent::__construct();
            // Your own constructor code
       }
      
      
      function getAll(){
        return 'id, shortname, longname, countrycode';
      
      }
      
      function getTablename()
      {
        return 'airline';
      }
    
    // quries for active briefings 
      
    function findByCountry($country)
    {                                     
        $this->db->select($this->getAll())->from($this->getTablename())->where(array('countrycode'=> $country) );
        return $this->db->get()->result(); 
    }
    function findById($id)
    {                                     
        $this->db->select($this->getAll())->from($this->getTablename())->where(array('id'=> $id) );
        return $this->db->get()->result(); 
    }
    
    function findAll()
    {                                     
        $this->db->select($this->getAll())->from($this->getTablename() );
        return $this->db->get()->result(); 
    }
    
    //TODO
    function search($shortstring){
        
        $sql = "select * from airline where lower(substr(shortname,1,?)) = ?";

        $query = $this->db->query($sql, array(strlen($shortstring), strtolower($shortstring))); 
       return $query->result();  
    }
    

    function insert_entry($shortname,$longname, $countrycode)
    {    
        $this->shortname = $shortname;
        $this->longname = $longname;
        $this->countrycode = $countrycode;      
        $this->db->insert($this->getTablename(), $this);
    }
    
    

    
    //Purge the record
    function delete_entry($id){
        $this->db->delete($this->getTablename(),array('id'=> $id));
    }

  
    
    function update_longname($id,$desc){    
         $data = array(
               'longname' => $desc,
              
            );
          
        $this->db->update($this->getTablename(), $data, array('id'=> $id));
    }
    
   
    
  }
?>
