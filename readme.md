RankAirline.com
---------------

I built this webapp/site to search and rate/rank airlines and nothing more. Sky is the limit so this is asking to get re-invented

  - Built real RESTful API for others to use data in json/xml/csv 
  - Complete ajax app with jquery accessing endpoints  
  - Experimented with great jquery based plugins, modified css3 styling for easy to read comments
  - basic, googlish approach to UX